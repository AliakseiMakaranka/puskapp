﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace PuskApp
{
    /// <summary>
    /// Interaction logic for OptionsWindow.xaml
    /// </summary>
    public partial class OptionsWindow : Window
    {
        private string _currentPath;
        private MainWindow _mainWindow;

        public OptionsWindow(MainWindow mainWindow)
        {
            InitializeComponent();

            WindowStyle = WindowStyle.None;
            AllowsTransparency = true;
            Background = new SolidColorBrush(Color.FromArgb(80, 110, 174, 247));

            InitHotKeys();
            Init();
            _mainWindow = mainWindow;
        }

        private void Init()
        {
            SetColorOfButtons();
            var primaryMonitorArea = SystemParameters.WorkArea;
            Left = primaryMonitorArea.Right - Width;
            Top = primaryMonitorArea.Bottom - Height - 60;
        }

        private void SetColorOfButtons()
        {
            for (int i = 0; i < buttonCombobox.Items.Count; i++)
            {
                ((ComboBoxItem)buttonCombobox.Items[i]).Background = Brushes.White;
            }

            var entities = WriterReader.Entities;

            foreach (var buttonAndAddress in entities)
            {
                ((ComboBoxItem)buttonCombobox.Items[buttonAndAddress.Button - 1]).Background = Brushes.GreenYellow;
            }
        }

        private void InitHotKeys()
        {
            RoutedCommand hideOption = new RoutedCommand();
            hideOption.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(hideOption, HideOptions));

            RoutedCommand quit = new RoutedCommand();
            quit.InputGestures.Add(new KeyGesture(Key.Q, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(quit, Quit));
        }

        private void HideOptions(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void Quit(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            FileButton.ClearValue(BackgroundProperty);
            FolderButton.ClearValue(BackgroundProperty);

            int buttonCount =int.Parse(buttonCombobox.SelectionBoxItem.ToString());
            WriterReader.AddEntity(new ButtonAndAddress{Button = buttonCount, Address = _currentPath});

            SetColorOfButtons();
            _currentPath = null;
            Helper.AddTooltipsAndIcons(_mainWindow);
        }

        private void Button_file_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            var find = ofd.ShowDialog();
            if (find == true)
            {
                FileInfo fi = new FileInfo(ofd.FileName);
                _currentPath = fi.FullName;
                FileButton.Background = Brushes.GreenYellow;
            }
        }

        private void Button_folder_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            CommonFileDialogResult result = dialog.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                FileInfo fi = new FileInfo(dialog.FileName);
                _currentPath = fi.FullName;
                FolderButton.Background = Brushes.GreenYellow;
            }
        }
    }
}
