﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace PuskApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GlobalKeyboardHook _gHook;
        private string _pattern = @"((162)+(160)+(112|113|114|115|116|117|118|119))|((160)+(162)+(112|113|114|115|116|117|118|119))";
        readonly StringBuilder _stringBuilder = new StringBuilder();
        
        public MainWindow()
        {
            InitializeComponent();


            WindowStyle = WindowStyle.None;
            AllowsTransparency = true;
            Background = new SolidColorBrush(Color.FromArgb(80, 110, 174, 247));


            InitHotKeys();
            Loaded += MyWindow_Loaded;
            Init();
        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _gHook = new GlobalKeyboardHook();
            _gHook.KeyDown += gHook_KeyDown;
            foreach (Keys key in Enum.GetValues(typeof(Keys)))
                _gHook.HookedKeys.Add(key);

            _gHook.hook();
        }

        public void gHook_KeyDown(object sender, System.Windows.Forms.KeyEventArgs keyEventArgs)
        {
            _stringBuilder.Append(keyEventArgs.KeyValue.ToString());

            var hotkeyFound = Regex.IsMatch(_stringBuilder.ToString(), _pattern, RegexOptions.IgnoreCase);
            if (hotkeyFound)
            {
                var buttonName = int.Parse(_stringBuilder.ToString().Last().ToString()) - 1;
                _stringBuilder.Clear();
                Button_Click(buttonName);
            }
            if (_stringBuilder.Length == 1000000)
            {
                _stringBuilder.Clear();
            }
        }

        private void Init()
        {
            WriterReader.Init();
            var primaryMonitorArea = SystemParameters.WorkArea;
            Left = primaryMonitorArea.Right - Width;
            Top = primaryMonitorArea.Bottom - Height;

            ToolTipService.SetInitialShowDelay(this, 1000);
            ToolTip = "Ctrl+O - Option Mode \r\n Ctrl+M - Minimize";

            Helper.AddTooltipsAndIcons(this);
        }

        private void InitHotKeys()
        {
            RoutedCommand minimize = new RoutedCommand();
            minimize.InputGestures.Add(new KeyGesture(Key.M, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(minimize, MinimizeToTray));

            RoutedCommand options = new RoutedCommand();
            options.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(options, SetOptions));

            RoutedCommand quit = new RoutedCommand();
            quit.InputGestures.Add(new KeyGesture(Key.Q, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(quit, Quit));
        }

        private void MinimizeToTray(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void SetOptions(object sender, RoutedEventArgs e)
        {
            OptionsWindow optionsWindow = new OptionsWindow(this);
            optionsWindow.Show();
        }

        private void Quit(object sender, RoutedEventArgs e)
        {
            _gHook.unhook();
            Application.Current.Shutdown();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(1);
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(2);
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(3);
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(4);
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(5);
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(6);
        }

        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(7);
        }

        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            Button_Click(8);
        }



        private void Button1_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 1);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this,entity);
                deleteWindow.Show();
            }
        }

        private void Button2_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 2);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this, entity);
                deleteWindow.Show();
            }
        }

        private void Button3_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 3);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this, entity);
                deleteWindow.Show();
            }
        }

        private void Button4_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 4);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this, entity);
                deleteWindow.Show();
            }
        }

        private void Button5_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 5);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this, entity);
                deleteWindow.Show();
            }
        }

        private void Button6_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 6);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this, entity);
                deleteWindow.Show();
            }
        }

        private void Button7_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 7);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this, entity);
                deleteWindow.Show();
            }
        }

        private void Button8_RClick(object sender, RoutedEventArgs e)
        {
            var entity = WriterReader.Entities.FirstOrDefault(x => x.Button == 8);
            if (entity != null)
            {
                Delete deleteWindow = new Delete(this, entity);
                deleteWindow.Show();
            }
        }

        private void Button_Click(int keyNum)
        {
            var address = WriterReader.Entities.FirstOrDefault(x => x.Button == keyNum)?.Address;
            try
            {
                if (address != null)
                    Process.Start(address);
            }
            catch (Win32Exception exception)
            {
                MessageBox.Show("Not Found on this PC");
            }
        }
    }
}
