﻿using System.Windows;
using System.Windows.Media;

namespace PuskApp
{
    /// <summary>
    /// Interaction logic for Delete.xaml
    /// </summary>
    public partial class Delete : Window
    {
        private ButtonAndAddress _entity;
        private MainWindow _mainWindow;

        public Delete(MainWindow mainWindow, ButtonAndAddress entity)
        {
            _entity = entity;
            InitializeComponent();

            WindowStyle = WindowStyle.None;
            AllowsTransparency = true;
            Background = new SolidColorBrush(Color.FromArgb(80, 110, 174, 247));

            var primaryMonitorArea = SystemParameters.WorkArea;
            Left = primaryMonitorArea.Right - Width;
            Top = primaryMonitorArea.Bottom - Height - 60;
            _mainWindow = mainWindow;
        }

        private void Button_Click_Yes(object sender, RoutedEventArgs e)
        {
            WriterReader.DeleteEntity(_entity);
            Helper.AddTooltipsAndIcons(_mainWindow);
            _entity = null;
            Hide();
        }

        private void Button_Click_No(object sender, RoutedEventArgs e)
        {
            _entity = null;
            Hide();
        }
    }
}
