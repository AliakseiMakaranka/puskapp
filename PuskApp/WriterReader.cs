﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;

namespace PuskApp
{
    public static class WriterReader
    {
        public static List<ButtonAndAddress> Entities { get; set; }

        public static void Init()
        {
            Entities = new List<ButtonAndAddress>();
            if (File.Exists(Directory.GetCurrentDirectory() + "\\Data.dat"))
            {

                using (FileStream fs = new FileStream("Data.dat", FileMode.OpenOrCreate))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    Entities = (List<ButtonAndAddress>)formatter.Deserialize(fs);
                }
            }
            else
            {
                using (new FileStream("Data.dat", FileMode.Create)) { }
            }
        }

        public static void AddEntity(ButtonAndAddress entity)
        {
            DeleteEntity(entity);

            if (entity.Address != null)
            {
                Entities.Add(entity);
                Serializer.Serialize();
            }
            else
            {
                MessageBox.Show("Address is empty!");
            }
        }

        public static void DeleteEntity(ButtonAndAddress entity)
        {
            var oldButtonInit = Entities.FirstOrDefault(x => x.Button == entity.Button);
            Entities.Remove(oldButtonInit);
            Serializer.Serialize();
        }
    }
}
