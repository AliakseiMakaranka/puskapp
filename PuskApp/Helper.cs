﻿using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace PuskApp
{
    public static class Helper
    {
        public static void AddTooltipsAndIcons(MainWindow mainWindow)
        {
            ClearAllTooltipsAndIcons(mainWindow);

            foreach (var entity in WriterReader.Entities)
            {
                if (entity.Address!= null)
                {
                Icon icon;
                FileAttributes attr = File.GetAttributes(entity.Address);

                if ((attr & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    icon = Icon.ExtractAssociatedIcon(entity.Address);
                }
                else
                {
                    icon = new Icon(@"..\..\Res\folder.ico");
                }

                switch (entity.Button)
                {
                    case 1:
                        mainWindow.Button1.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg1.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    case 2:
                        mainWindow.Button2.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg2.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    case 3:
                        mainWindow.Button3.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg3.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    case 4:
                        mainWindow.Button4.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg4.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    case 5:
                        mainWindow.Button5.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg5.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    case 6:
                        mainWindow.Button6.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg6.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    case 7:
                        mainWindow.Button7.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg7.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    case 8:
                        mainWindow.Button8.ToolTip = entity.Address;
                        if (icon != null)
                            mainWindow.ButtonImg8.Source = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                        break;
                    default:
                        break;
                }
                }
            }
        }

        public static void ClearAllTooltipsAndIcons(MainWindow mainWindow)
        {
            mainWindow.Button1.ToolTip = null;
            mainWindow.ButtonImg1.Source = null;

            mainWindow.Button2.ToolTip = null;
            mainWindow.ButtonImg2.Source = null;

            mainWindow.Button3.ToolTip = null;
            mainWindow.ButtonImg3.Source = null;

            mainWindow.Button4.ToolTip = null;
            mainWindow.ButtonImg4.Source = null;

            mainWindow.Button5.ToolTip = null;
            mainWindow.ButtonImg5.Source = null;

            mainWindow.Button6.ToolTip = null;
            mainWindow.ButtonImg6.Source = null;

            mainWindow.Button7.ToolTip = null;
            mainWindow.ButtonImg7.Source = null;

            mainWindow.Button8.ToolTip = null;
            mainWindow.ButtonImg8.Source = null;
        }
    }
}
