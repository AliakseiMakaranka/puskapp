﻿using System;

namespace PuskApp
{
    [Serializable]
    public class ButtonAndAddress
    {
        public int Button { get; set; }
        public string Address { get; set; }
    }
}
