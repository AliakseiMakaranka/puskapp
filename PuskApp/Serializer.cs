﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace PuskApp
{
    public static class Serializer
    {
        public static void Serialize()
        {
            FileStream fs = new FileStream("Data.dat", FileMode.OpenOrCreate);

            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, WriterReader.Entities);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }


        public static List<ButtonAndAddress> Deserialize()
        {
            List<ButtonAndAddress> entityes = new List<ButtonAndAddress>();
            
                FileStream fs = new FileStream("Data.dat", FileMode.Open);
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    entityes = (List<ButtonAndAddress>)formatter.Deserialize(fs);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                    throw;
                }
                finally
                {
                    fs.Close();
                }

            return entityes;
        }
    }
}
